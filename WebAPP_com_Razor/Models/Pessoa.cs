﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPP_com_Razor.Models
{
    public class Pessoa
    {
        public int PessoaId { get; set; }
        public string Nome { get; set; }
        public int Idade { get; set; }
    }
}