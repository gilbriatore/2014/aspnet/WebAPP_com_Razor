﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAPP_com_Razor.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var p1 = new WebAPP_com_Razor.Models.Pessoa();
            p1.Nome = "João";
            p1.Idade = 15;

            var p2 = new WebAPP_com_Razor.Models.Pessoa();
            p2.Nome = "Pedro";
            p2.Idade = 25;

            List<WebAPP_com_Razor.Models.Pessoa> lista =
                new List<WebAPP_com_Razor.Models.Pessoa>();

            lista.Add(p1);
            lista.Add(p2);

            //ViewData["nomeDalista"] = lista;

            return View(lista);
        }
	}
}